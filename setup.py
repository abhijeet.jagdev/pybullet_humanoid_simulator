from setuptools import setup


setup(name = 'balance_bot',
      version = '0.0.3',
      install_requires = ['gym',
                          'pybullet',
			  'tensorflow']
)


setup(name = 'deepmimic_humanoid',
      version = '0.0.1',
      install_requires = ['gym',
                          'pybullet',
                          'tensorflow']
)
