"""
    Generic Stable-Baselines Based Training Simulator
    =================================================

    Supports the direct usage of the following DRL algorithms:

        - PPO2
        - A2C
        - TRPO
        - ACKTR (discrete action space only)

    ** NOTE: this script assumes the custom environments to be
    OpenAI Gym compliant but also requires a "enable_draw()" 
    method for rendering purposes. The DeepMimicHumanoid-v0 
    environment additionally requires a "set_simulation_parameters()"
    method to set the terrain and motion file through this scripts
    interface.

    @author: Abhijeet Singh Jagdev
    @references:

    [1] https://stable-baselines.readthedocs.io/en/master/index.html
    [2] https://gym.openai.com/
    [3] https://backyardrobotics.eu/2017/11/29/build-a-balancing-bot-with-openai-gym-pt-ii-the-robot-and-environment/

"""

# General Imports

import os
import argparse
import time
import numpy as np
import matplotlib.pyplot as plt

# OpenAI Gym Imports

import gym

# Pybullet Imports

import pybullet_envs

# Stable Baselines Utilities

from stable_baselines.common.vec_env.dummy_vec_env import DummyVecEnv
from stable_baselines.bench import Monitor
from stable_baselines.results_plotter import load_results, ts2xy
from stable_baselines.ddpg.noise import AdaptiveParamNoiseSpec

# Stable Baselines Agents

from stable_baselines import PPO2
from stable_baselines import A2C
from stable_baselines import TRPO
from stable_baselines import ACKTR

# Stable Baselines Policies

from stable_baselines.common.policies import MlpPolicy

# Custom Environments

import balance_bot
import deepmimic_humanoid

################################################### GLOBAL VARIABLES ###################################################

best_mean_reward, n_steps = -np.inf, 0
save_path = None

################################################### HELPER FUNCTIONS ###################################################

def callback(_locals, _globals):

    """
        Callback function called at each update step. Every 1000 steps, this function reports 
        the best mean reward and last mean reward over the last 100 episodes and saves the 
        intermediate model if it achieved a higher mean reward than the current best.

        Parameters
        ----------
        _locals
            a dict of local variables
        _globals
            a dict of global variables

        Returns
        -------
        True

    """

    global n_steps, best_mean_reward, save_path

    # print stats every 1000 calls

    if (n_steps + 1) % 1000 == 0:

      # evaluate policy performance

      x, y = ts2xy(load_results(save_path), 'timesteps')

      if len(x) > 0:
          mean_reward = np.mean(y[-100:])
          print(x[-1], 'timesteps')
          print(">>> BEST MEAN REWARD: {:.2f} \n>>> LAST MEAN REWARD: {:.2f}".format(best_mean_reward, mean_reward))

          # new best model, save the agent

          if mean_reward > best_mean_reward:
              best_mean_reward = mean_reward
              print(">> SAVING NEW BEST INTERMEDIATE MODEL: " + save_path + '/best_model.pkl')
              _locals['self'].save(os.path.join(save_path, 'best_model.pkl'))

    n_steps += 1

    return True

##################################################### MAIN FUNCTION ####################################################

def main(args):

    """
        Humanoid Training Simulator: builds an OpenAI gym enviroment for either training or simulation purposes.
        Functionality is controlled by the command-line arguments.

        Parameters
        ----------
        args
            an ArgParse object containing the parsed command-line arguments

        Returns
        -------
        None

    """

    print("\n################################# HUMANOID TRAINING SIMULATION #################################\n")

    global save_path
    curr_timestep, start_time = 0, time.time()

    if args.mode.lower().strip() == 'train':

        # create log directory

        timestamp = time.gmtime()
        formatted_timestamp = time.strftime("%Y_%m_%d_%H_%M_%S", timestamp)
        save_path = os.path.join(args.log_dir, args.env, args.algorithm, formatted_timestamp)
        os.makedirs(save_path, exist_ok = True)
        print(">>> CREATED LOG DIRECTORY: " + save_path)

    # create and wrap the environment

    env = gym.make(args.env)

    if args.env == 'DeepMimicHumanoid-v0':
        timestep = 1./args.timestep_denominator
        env.set_simulation_parameters(timestep, args.motion_file, args.terrain_file)

    if args.mode.lower().strip() == 'train':
        env.enable_draw(args.render)
    else:
        env.enable_draw(True)

    env = Monitor(env, save_path, allow_early_resets = True)
    env = DummyVecEnv([lambda: env])
    print(">>> CREATED ENVIRONMENT: " + args.env)

    if args.mode.lower().strip() == 'train':

        print("\n################################## LAUNCHING TRAINING SESSION ##################################\n")

        # define model and initiate training

        model = None

        if args.algorithm == 'PPO':
            model = PPO2(MlpPolicy, env, verbose = True, tensorboard_log = save_path + '/tensorboard/')
        elif args.algorithm == 'TRPO':
            model = TRPO(MlpPolicy, env, verbose = True, tensorboard_log = save_path + '/tensorboard/')
        elif args.algorithm == 'A2C':
            model = A2C(MlpPolicy, env, verbose = True, tensorboard_log = save_path + '/tensorboard/')
        elif args.algorithm == 'ACKTR':
            model = ACKTR(MlpPolicy, env, verbose = True, tensorboard_log = save_path + '/tensorboard/')
        else:
            print(">>> ERROR: unsupported algorithm specified.\n")
            return

        if not model == None:
            model.learn(total_timesteps = args.timesteps, callback = callback)
            print(">>> COMPLETED TRAINING MODEL")

            # save model

            model.save(os.path.join(save_path, 'final_model.pkl'))
            print(">>> SAVED MODEL: " + save_path + '/final_model.pkl\n')

        else:
            print(">>> ERROR: model could not be created.")

    elif args.mode.lower().strip() == 'play':

        print("\n##################################### LAUNCHING SIMULATION #####################################\n")

        # load model and render environment

        model = None

        if not args.model_path == None:

            if args.algorithm == 'PPO':
                model = PPO2.load(args.model_path)
            elif args.algorithm == 'TRPO':
                model = TRPO.load(args.model_path)
            elif args.algorithm == 'A2C':
                model = A2C.load(args.model_path)
            elif args.algorithm == 'ACKTR':
                model = ACKTR.load(args.model_path)
            else:
                print(">>> ERROR: unsupported algorithm specified.\n")
                return

        else:
            print(">>> ERROR: model file not specified.\n")
            return

        # enjoy trained agent

        print(">>> LOADED MODEL: " + args.model_path + "\n")

        env.render(mode = "human")
        print(">>> RENDERING SIMULATION ...")

        for episode in range(args.episodes):

            # reset environment at beginning of episode

            observation = env.reset()

            # run episode until max steps per episode limit reached or agent enters a terminal state

            for t in range(args.max_steps):

                # break if timestep limit reached

                if curr_timestep > args.timesteps:
                    print("\n\n>>> MAX TIMESTEP LIMIT OF " + str(args.timesteps) + " REACHED")
                    return

                # render environment

                action, _states = model.predict(observation)
                observation, reward, done, info = env.step(action)
                env.render(mode = "human")
                curr_timestep += 1

                if done:

                    # agent entered a terminal state, finishing episode

                    curr_global_timestep = "GLOBAL TIMESTEP: " + str(curr_timestep) + " | "
                    curr_episode = "EPISODE: " + str(episode + 1) + " | "
                    curr_episode_timestep = " finished after " + str(t + 1) + " timesteps" + " | "
                    curr_time = "elapsed time = " + "{0:.2f}".format(float(time.time() - start_time)) + " s"

                    print(curr_global_timestep + curr_episode + curr_episode_timestep + curr_time, end = '\r')
                    
                    break

        print("\n\n")

    else:
        print(">>> ERROR: invalid mode specified.\n")
        return


if __name__ == '__main__':

    """
        Command Line Arguments
        ----------------------
        env
            OpenAI gym environment to use (default = 'HumanoidBulletEnv-v0').
        timestep_denominator
            The denominator value in the time equivalence of each step (default = 600).
            Therefore, the each step is equivalent to 1/600 seconds.
        timesteps
            Max # of total timesteps to run simulation for (default = 10000).
        max_steps
            Max # of timesteps per episode (default = 10000).
        episodes
            Max # of episodes to run simulation for (default = 100).
        algorithm
            Deep reinforcement learning algorithm used to learn action policy (default = PPO).
            Available options: [ 'PPO','A2C','TRPO','ACKTR' ]
        mode
            Mode in which this script is run:
            - play: renders environment and loads pre-trained agent
            - train: trains an action policy using specified algorithm and environment
        log_dir
            Directory to which model and training logs are saved (default = './logs/').
        model_path
            Path to the serialized model file that will be loaded for running simulation.
        record [DISABLED]
            Record a GIF of the simulation or not (enter 'true')
        render
            Select whether to render simulation while training (not recommended as it slows down
            training considerably)
        motion_file
            Name of the reference motion file used in training (expected to be located in 
            ./deepmimic_humanoid/envs/motions/). [include file extension]
        terrain_file
            Name of the terrain file used in the simulation (expected to be located in 
            ./deepmimic_humanoid/envs/terrains/) along with any other required static assets. 
            [include file extension]

    """

    parser = argparse.ArgumentParser(description = 'Simulation Environment Parameters')

    parser.add_argument("-E", "--env", help = "OpenAI gym environment to use (default = 'DeepMimicHumanoid-v0')", default = "DeepMimicHumanoid-v0")
    parser.add_argument("-t", "--timestep_denominator", help = "time equivalence of each step as 1./<timestep_denominator> (default = 600)", type = int, default = 600)
    parser.add_argument("-T", "--timesteps", help = "max # of total timesteps to run simulation for (default = 200000)", type = int, default = 200000)
    parser.add_argument("-M", "--max_steps", help = "max # of timesteps per episode (default = 10000)", type = int, default = 10000)
    parser.add_argument("-e", "--episodes", help = "max # of episodes to run simulation for (default = 100)", type = int, default = 100)
    parser.add_argument("-A", "--algorithm", help = "deep reinforcement learning algorithm used to learn action policy (default = PPO)", default = 'PPO')
    parser.add_argument("-m", "--mode", help = "running mode ['play', 'train'] (default = 'train')", default = 'train')
    parser.add_argument("-L", "--log_dir", help = "directory to which model and training logs are saved (default = './logs/')", default = './logs')
    parser.add_argument("-l", "--model_path", help = "path to model .pkl file to load for running simulation", default = None)
    parser.add_argument("-R", "--record", help = "record a GIF of the simulation (saved to directory containing model)", default = None)
    parser.add_argument("-r", "--render", help = "render the simulation while training (default = 0)", type = int, default = 0)
    parser.add_argument("-x", "--motion_file", help = "motion file to train DeepMimicHumanoid-v0 on (default = 'humanoid3d_backflip.txt')", default = 'humanoid3d_backflip.txt')
    parser.add_argument("-y", "--terrain_file", help = "terrain file to train DeepMimicHumanoid-v0 on (default = 'plane_implicit.urdf')", default = 'plane_implicit.urdf')

    args = parser.parse_args()

    main(args)