"""
    This humanoid environment class is adapted from the DeepMimic StablePD humanoid environment
    (implemented in PyBullet). This environment only contains the methods necessary to 
    replay the reference motion data. See file "motion_player.py" which instantiates this 
    environment and runs the Pybullet simulation.

"""

import math
import pybullet as pb
import pybullet_data

from pybullet_envs.deep_mimic.env import humanoid_pose_interpolator
from motion_capture_data import MotionCaptureData
from pybullet_utils import bullet_client

# URDF-based link indices (wrist links not used)

chest = 1
neck = 2
rightHip = 3
rightKnee = 4
rightAnkle = 5
rightShoulder = 6
rightElbow = 7
leftHip = 9
leftKnee = 10
leftAnkle = 11
leftShoulder = 12
leftElbow = 13
jointFrictionForce = 0

class HumanoidStablePD(object):

    def __init__(self, pybullet_client, mocap_data, timeStep, useFixedBase = True):

        """
            This method loads the humanoid URDF, defines the collision properties, dynamics
            and joint controls of the humanoid. The motion data is wrapped in a 
            MotionCaptureData abstraction to easily compute poses given a time value T.
        """

        pb.setAdditionalSearchPath(pybullet_data.getDataPath())

        print(">>> LOADING HUMANOID")

        self._pybullet_client = pybullet_client
        self._mocap_data = mocap_data 

        # define kinematic model: character that uses reference motion capture data

        self._kin_model = self._pybullet_client.loadURDF(
            "humanoid/humanoid.urdf", 
            [0, 0.85, 0],
            globalScaling = 0.25, 
            useFixedBase = True, 
            flags = self._pybullet_client.URDF_MAINTAIN_LINK_ORDER)

        # define dynamics of kinematic model's base (index = -1) and joints

        self._pybullet_client.changeDynamics(self._kin_model, -1, linearDamping = 0, angularDamping = 0)
        self._pybullet_client.setCollisionFilterGroupMask(self._kin_model, -1, collisionFilterGroup = 0, 
                                                          collisionFilterMask = 0)
        self._pybullet_client.changeDynamics(self._kin_model, -1, 
            activationState = self._pybullet_client.ACTIVATION_STATE_SLEEP + 
                              self._pybullet_client.ACTIVATION_STATE_ENABLE_SLEEPING + 
                              self._pybullet_client.ACTIVATION_STATE_DISABLE_WAKEUP)

        for j in range (self._pybullet_client.getNumJoints(self._kin_model)):
            self._pybullet_client.setCollisionFilterGroupMask(self._kin_model, j, 
                                                              collisionFilterGroup = 0, 
                                                              collisionFilterMask = 0)
            self._pybullet_client.changeDynamics(self._kin_model, j, 
                                                 activationState = self._pybullet_client.ACTIVATION_STATE_SLEEP + 
                                                                   self._pybullet_client.ACTIVATION_STATE_ENABLE_SLEEPING + 
                                                                   self._pybullet_client.ACTIVATION_STATE_DISABLE_WAKEUP)

        self._jointIndicesAll = [chest, neck, 
                                 rightHip, rightKnee, rightAnkle, rightShoulder, rightElbow, 
                                 leftHip, leftKnee, leftAnkle, leftShoulder, leftElbow]

        # define motor controls for all joints

        for j in self._jointIndicesAll:
            self._pybullet_client.setJointMotorControl2(self._kin_model, j, 
                                                        self._pybullet_client.POSITION_CONTROL, 
                                                        targetPosition = 0, 
                                                        positionGain = 0, 
                                                        targetVelocity = 0,
                                                        force = 0)
            self._pybullet_client.setJointMotorControlMultiDof(self._kin_model, j, 
                                                               self._pybullet_client.POSITION_CONTROL,
                                                               targetPosition = [0,0,0,1], 
                                                               targetVelocity = [0,0,0], 
                                                               positionGain = 0,
                                                               velocityGain = 1,
                                                               force = [jointFrictionForce, jointFrictionForce, 0])

        # process motion capture data using PoseInterpolator

        self._poseInterpolator = humanoid_pose_interpolator.HumanoidPoseInterpolator()

        for i in range (self._mocap_data.NumFrames()-1):
            frameData = self._mocap_data._motion_data['Frames'][i]
            self._poseInterpolator.PostProcessMotionData(frameData)

        # initialize environment
          
        self._timeStep = timeStep
        self.setSimTime(0)
        self.resetPose()

    ################################ DETERMINE POSE AT TIME T ################################

    def setSimTime(self, t):

        self._kinTime = t
        keyFrameDuration = self._mocap_data.KeyFrameDuraction()
        cycleTime = self.getCycleTime()
        self._cycleCount = self.calcCycleCount(t, cycleTime)
        frameTime = t - self._cycleCount * cycleTime

        if (frameTime < 0):
            frameTime += cycleTime

        self._frame = int(frameTime/keyFrameDuration)
        self._frameNext = self._frame+1

        if (self._frameNext >=  self._mocap_data.NumFrames()):
            self._frameNext = self._frame

        self._frameFraction = (frameTime - self._frame * keyFrameDuration) / (keyFrameDuration)


    def calcCycleCount(self, simTime, cycleTime):
        
        phases = simTime / cycleTime;
        count = math.floor(phases)
        loop = True
        
        return count


    def getCycleTime(self):

        keyFrameDuration = self._mocap_data.KeyFrameDuraction()
        cycleTime = keyFrameDuration * (self._mocap_data.NumFrames() - 1)
        
        return cycleTime

    def computePose(self, frameFraction):
        
        frameData = self._mocap_data._motion_data['Frames'][self._frame]
        frameDataNext = self._mocap_data._motion_data['Frames'][self._frameNext]

        self._poseInterpolator.Slerp(frameFraction, frameData, frameDataNext, self._pybullet_client)
        self.computeCycleOffset()
        
        oldPos = self._poseInterpolator._basePos
        
        self._poseInterpolator._basePos = [oldPos[0] + self._cycleCount * self._cycleOffset[0],
                                           oldPos[1] + self._cycleCount * self._cycleOffset[1],
                                           oldPos[2] + self._cycleCount * self._cycleOffset[2]]
        
        pose = self._poseInterpolator.GetPose()

        return pose

    def computeCycleOffset(self):
        
        firstFrame = 0
        lastFrame = self._mocap_data.NumFrames() - 1
        
        frameData = self._mocap_data._motion_data['Frames'][0]
        frameDataNext = self._mocap_data._motion_data['Frames'][lastFrame]
        
        basePosStart = [frameData[1], frameData[2], frameData[3]]
        basePosEnd = [frameDataNext[1], frameDataNext[2], frameDataNext[3]]
        
        self._cycleOffset = [basePosEnd[0] - basePosStart[0], 
                             basePosEnd[1] - basePosStart[1],
                             basePosEnd[2] - basePosStart[2]]
        
        return self._cycleOffset

    ################################## SET CHARACTER TO POSE #################################

    def resetPose(self):

        """
            Determine the pose given current time cycle (frame fraction) and initialize model
            to the current pose. Typically, the time value when using this method will be 0.
        """

        pose = self.computePose(self._frameFraction)
        self.initializePose(self._poseInterpolator, self._kin_model, initBase = True)
    
    def initializePose(self, pose, phys_model, initBase, initializeVelocity = True):

        """
            Set model joint to rotation (and optionally velocity) values given pose vector.
        """

        if initializeVelocity:
            if initBase:
                self._pybullet_client.resetBasePositionAndOrientation(phys_model, pose._basePos, pose._baseOrn)
                self._pybullet_client.resetBaseVelocity(phys_model, pose._baseLinVel, pose._baseAngVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, chest, pose._chestRot, pose._chestVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, neck, pose._neckRot, pose._neckVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightHip, pose._rightHipRot, pose._rightHipVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightKnee, pose._rightKneeRot, pose._rightKneeVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightAnkle, pose._rightAnkleRot, pose._rightAnkleVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightShoulder, pose._rightShoulderRot, pose._rightShoulderVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightElbow, pose._rightElbowRot, pose._rightElbowVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftHip, pose._leftHipRot, pose._leftHipVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftKnee, pose._leftKneeRot, pose._leftKneeVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftAnkle, pose._leftAnkleRot, pose._leftAnkleVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftShoulder, pose._leftShoulderRot, pose._leftShoulderVel)
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftElbow, pose._leftElbowRot, pose._leftElbowVel)
        else:
            if initBase:
                self._pybullet_client.resetBasePositionAndOrientation(phys_model, pose._basePos, pose._baseOrn)
            self._pybullet_client.resetJointStateMultiDof(phys_model, chest, pose._chestRot, [0,0,0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, neck, pose._neckRot, [0,0,0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightHip, pose._rightHipRot, [0,0,0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightKnee, pose._rightKneeRot, [0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightAnkle, pose._rightAnkleRot, [0,0,0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightShoulder, pose._rightShoulderRot, [0,0,0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, rightElbow, pose._rightElbowRot, [0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftHip, pose._leftHipRot, [0,0,0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftKnee, pose._leftKneeRot, [0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftAnkle, pose._leftAnkleRot, [0,0,0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftShoulder, pose._leftShoulderRot, [0,0,0])
            self._pybullet_client.resetJointStateMultiDof(phys_model, leftElbow, pose._leftElbowRot, [0])


    def getState(self):

        stateVector = []
        phase = self.getPhase()
        #print("phase=",phase)
        stateVector.append(phase)
        
        rootTransPos, rootTransOrn=self.buildOriginTrans()
        basePos,baseOrn = self._pybullet_client.getBasePositionAndOrientation(self._kin_model)
        
        rootPosRel, dummy = self._pybullet_client.multiplyTransforms(rootTransPos, rootTransOrn, basePos,[0,0,0,1])
        #print("!!!rootPosRel =",rootPosRel )
        #print("rootTransPos=",rootTransPos)
        #print("basePos=",basePos)
        localPos,localOrn = self._pybullet_client.multiplyTransforms( rootTransPos, rootTransOrn , basePos,baseOrn )
        
        localPos=[localPos[0]-rootPosRel[0],localPos[1]-rootPosRel[1],localPos[2]-rootPosRel[2]]
        #print("localPos=",localPos)
            
        stateVector.append(rootPosRel[1])
        
        #self.pb2dmJoints=[0,1,2,9,10,11,3,4,5,12,13,14,6,7,8]
        self.pb2dmJoints=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]
        
        for pbJoint in range (self._pybullet_client.getNumJoints(self._kin_model)):
            j = self.pb2dmJoints[pbJoint]
            #print("joint order:",j)
            ls = self._pybullet_client.getLinkState(self._kin_model, j, computeForwardKinematics=True)
            linkPos = ls[0]
            linkOrn = ls[1]
            linkPosLocal, linkOrnLocal = self._pybullet_client.multiplyTransforms(rootTransPos, rootTransOrn, linkPos,linkOrn)
            if (linkOrnLocal[3]<0):
                linkOrnLocal=[-linkOrnLocal[0],-linkOrnLocal[1],-linkOrnLocal[2],-linkOrnLocal[3]]
            linkPosLocal=[linkPosLocal[0]-rootPosRel[0],linkPosLocal[1]-rootPosRel[1],linkPosLocal[2]-rootPosRel[2]]
            for l in linkPosLocal:
                stateVector.append(l)
            #re-order the quaternion, DeepMimic uses w,x,y,z

            if (linkOrnLocal[3]<0):
                linkOrnLocal[0]*=-1
                linkOrnLocal[1]*=-1
                linkOrnLocal[2]*=-1
                linkOrnLocal[3]*=-1

            stateVector.append(linkOrnLocal[3])
            stateVector.append(linkOrnLocal[0])
            stateVector.append(linkOrnLocal[1])
            stateVector.append(linkOrnLocal[2])
        
        
        for pbJoint in range (self._pybullet_client.getNumJoints(self._kin_model)):
            j = self.pb2dmJoints[pbJoint]
            ls = self._pybullet_client.getLinkState(self._kin_model, j, computeLinkVelocity=True)
            linkLinVel = ls[6]
            linkAngVel = ls[7]
            #print(ls)
            linkLinVelLocal  , unused = self._pybullet_client.multiplyTransforms([0,0,0], rootTransOrn, linkLinVel,[0,0,0,1])
            #linkLinVelLocal=[linkLinVelLocal[0]-rootPosRel[0],linkLinVelLocal[1]-rootPosRel[1],linkLinVelLocal[2]-rootPosRel[2]]
            linkAngVelLocal ,unused = self._pybullet_client.multiplyTransforms([0,0,0], rootTransOrn, linkAngVel,[0,0,0,1])

            for l in linkLinVelLocal:
                stateVector.append(l)
            for l in linkAngVelLocal:
                stateVector.append(l)

            #print("stateVector len=",len(stateVector))  
            #for st in range (len(stateVector)):
            #  print("state[",st,"]=",stateVector[st])

        return stateVector
    
    def getPhase(self):

        keyFrameDuration = self._mocap_data.KeyFrameDuraction()
        cycleTime = keyFrameDuration*(self._mocap_data.NumFrames()-1)
        phase = self._kinTime / cycleTime
        phase = math.fmod(phase,1.0)
        
        if (phase < 0):
            phase += 1
        
        return phase
    
    def buildOriginTrans(self):
        rootPos,rootOrn = self._pybullet_client.getBasePositionAndOrientation(self._kin_model)

        #print("rootPos=",rootPos, " rootOrn=",rootOrn)
        invRootPos=[-rootPos[0], 0, -rootPos[2]]
        #invOrigTransPos, invOrigTransOrn = self._pybullet_client.invertTransform(rootPos,rootOrn)
        headingOrn = self.buildHeadingTrans(rootOrn)
        #print("headingOrn=",headingOrn)
        headingMat = self._pybullet_client.getMatrixFromQuaternion(headingOrn)
        #print("headingMat=",headingMat)
        #dummy, rootOrnWithoutHeading = self._pybullet_client.multiplyTransforms([0,0,0],headingOrn, [0,0,0], rootOrn)
        #dummy, invOrigTransOrn = self._pybullet_client.multiplyTransforms([0,0,0],rootOrnWithoutHeading, invOrigTransPos, invOrigTransOrn)

        invOrigTransPos, invOrigTransOrn = self._pybullet_client.multiplyTransforms( [0,0,0],headingOrn, invRootPos,[0,0,0,1])
        #print("invOrigTransPos=",invOrigTransPos)
        #print("invOrigTransOrn=",invOrigTransOrn)
        invOrigTransMat = self._pybullet_client.getMatrixFromQuaternion(invOrigTransOrn)
        #print("invOrigTransMat =",invOrigTransMat )
        return invOrigTransPos, invOrigTransOrn
    
    def buildHeadingTrans(self, rootOrn):
        #align root transform 'forward' with world-space x axis
        eul = self._pybullet_client.getEulerFromQuaternion(rootOrn)
        refDir = [1,0,0]
        rotVec = self._pybullet_client.rotateVector(rootOrn, refDir)
        heading = math.atan2(-rotVec[2], rotVec[0])
        heading2=eul[1]
        #print("heading=",heading)
        headingOrn = self._pybullet_client.getQuaternionFromAxisAngle([0,1,0],-heading)
        return headingOrn


