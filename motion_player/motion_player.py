"""
    This ultility script that provides an interface for visualizing the DeepMimic reference motion 
    data available for DeepMicmic Pybullet environments. Currently, only the humanoid environment
    is supported.

    CONTROLS
    --------

                   PLAY/PAUSE TOGGLE: spacebar
          STEP FORWARDS CONTINUOUSLY: x
         STEP BACKWARDS CONTINUOUSLY: z
                  STEP FORWARDS ONCE: s
                 STEP BACKWARDS ONCE: a
                                EXIT: escape OR terminate program from command line (control + C)

"""

import argparse
import pybullet as pb
import math
import pybullet_data

from motion_capture_data import MotionCaptureData
from pybullet_utils import bullet_client

# environments

from deepmimic_humanoid_visualization import HumanoidStablePD

pybullet_client = None
animating = single_step_forwards = single_step_backwards = step_forwards = step_backwards = False

def main(args):

    global pybullet_client, animating, single_step_forwards, single_step_backwards, step_forwards, step_backwards

    timestep = 1./args.timestep_denominator

    # initialize PyBullet physics client

    pybullet_client = bullet_client.BulletClient(connection_mode = pb.GUI)
    pybullet_client.configureDebugVisualizer(pybullet_client.COV_ENABLE_Y_AXIS_UP, 1)
    pybullet_client.setGravity(0,-9.8,0)
    pybullet_client.setPhysicsEngineParameter(numSolverIterations = 10)
    pybullet_client.setTimeStep(timestep)
    pybullet_client.setPhysicsEngineParameter(numSubSteps = 1)
    time_ID = pybullet_client.addUserDebugParameter("Time", 0, 10, 0)
    pb.setAdditionalSearchPath(pybullet_data.getDataPath())
    print("CONFIGURED PYBULLLET GUI CLIENT")

    # load terrain file
    
    z2y = pybullet_client.getQuaternionFromEuler([-math.pi * 0.5, 0, 0])
    plane_ID = pybullet_client.loadURDF(args.terrain_file, [0,0,0], z2y, useMaximalCoordinates = True)
    pybullet_client.changeDynamics(plane_ID, linkIndex = -1, lateralFriction = 0.9)
    print("LOADED TERRAIN FILE: " + str(args.terrain_file.split('/')[-1]))  

    # load motion data file

    motion_data = MotionCaptureData()
    motion_data.Load(args.motion_file)
    print("LOADED REFERENCE MOTION DATA: " + str(args.motion_file.split('/')[-1]))

    # define model environment

    t = 0
    env = None

    if args.env == 'deepmimic_humanoid':
        env = HumanoidStablePD(pybullet_client, motion_data, timestep, False)
        env.setSimTime(t)
        env.resetPose()
        print("LOADED ENVIRONMENT: " + str(args.env) + "\n")
    
    while (1):

        keys = pybullet_client.getKeyboardEvents()
        
        if is_key_triggered(keys, ' '):
            animating = not animating

        if is_key_triggered(keys, 's'):
            single_step_forwards = True

        if is_key_triggered(keys, 'a'):
            single_step_backwards = True

        if is_key_down(keys, 'x'):
            step_forwards = True

        if is_key_down(keys, 'z'):
            step_backwards = True

        if animating or single_step_forwards or single_step_backwards or step_forwards or step_backwards:

            print("T = " + str(t)[:5], end = "\r")
            
            if t >= float(args.reset_frequency):
                env.resetPose()
                t = 2./600.

            if t < 2./600.:
                t = args.reset_frequency - 1./600.
            
            pybullet_client.setTimeStep(timestep)
            env._timeStep = timestep

            if single_step_backwards or step_backwards:
                t -= timestep
            else:
                t += timestep

            env.setSimTime(t)
            kinPose = env.computePose(env._frameFraction)
            env.initializePose(env._poseInterpolator, env._kin_model, initBase = True)
            pybullet_client.stepSimulation()
            state = env.getState()
            #print(state)

            single_step_forwards = False
            single_step_backwards = False

            step_forwards = False
            step_backwards = False


def is_key_triggered(keys, key):
    o = ord(key)
    if o in keys:
        return keys[ord(key)] & pybullet_client.KEY_WAS_TRIGGERED
    return False

def is_key_down(keys, key):
    o = ord(key)
    if o in keys:
        return keys[ord(key)] & pybullet_client.KEY_IS_DOWN
    return False


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = "Reference Motion Player")

    parser.add_argument("-M", "--motion_file", 
        help = "path to motion data file (default = './motions/humanoid3d_backflip.txt')", 
        default = './motions/humanoid3d_backflip.txt')
    parser.add_argument("-T", "--terrain_file",
        help = "terrain file (default = './terrains/plane_implicit.urdf')",
        default = './terrains/plane_implicit.urdf')
    parser.add_argument("-E", "--env",
        help = "environment (default = 'deepmimic_humanoid')",
        default = 'deepmimic_humanoid')
    parser.add_argument("-t", "--timestep_denominator",
        help = "timestep increment controlling play-back speed in format 1./<timestep_denominator> (default = 600)",
        type = int,
        default = 600)
    parser.add_argument("-F", "--reset_frequency",
        help = "reset motion every X timestep increments (default = 10.)",
        type = int,
        default = 10)

    args = parser.parse_args()
    main(args)