from deepmimic_humanoid_env import DeepMimicHumanoid
import numpy as np

timestep = 1./60.

env = DeepMimicHumanoid()
env.set_simulation_parameters(timestep)
env.enable_draw(True)


t = 0
obs = None

while True:
    
    t += timestep
    
    if t > 2.:
        env.reset()
        t = 0.
    
    obs, reward, done, _ = env.step(np.random.uniform(low = -1, high = 1, size = (36,)).tolist())
    
    print(reward)