"""
    DeepMimic-Base OpenAI Gym Compliant Humanoid Environment
    ========================================================

    This is a OpenAI Gym compliant environment adapted from the
    Pybullet implementation of DeepMimic.

    This version only supports reference motion-based rewards
    (whereas original DeepMimic framework additionally incorporates
    goal-based rewards). Furthermore, the reward calculations need 
    to be updated to factor in non-plane terrains when appropriate.

    @author: Abhijeet Singh Jagdev
    @references: 
    
    [1] https://github.com/bulletphysics/bullet3/tree/master/examples/pybullet/gym/pybullet_envs/deep_mimic

"""


import math
import numpy as np
import gym
import pybullet as pb
import pybullet_data
import json
import os
import random

from deepmimic_humanoid.envs import motion_capture_data
from deepmimic_humanoid.envs import humanoid_pose_interpolator
from gym import spaces
from pybullet_utils import bullet_client
from pybullet_utils import pd_controller_stable

class DeepMimicHumanoid(gym.Env):

    #################################################################################
    # ==== Override Methods to Comply With an OpenAI Gym Environment Interface ==== #
    #################################################################################

    def __init__(self, render = False):
        
        """
            Initializes the OpenAI Gym Environment by:
            
                - loading model joint metadata
                - defining action & observations spaces
        
        """

        self.load_model_joint_metadata()

        # define action space
        self.action_space = spaces.Box(np.array(self.lower_bound), np.array(self.upper_bound), dtype = np.float32)

        # define observation space
        self.observation_space = spaces.Box(low = -5.0, high = 5.0, shape = (197,), dtype = np.float32)

    def set_simulation_parameters(self, timestep = 1./600., motion_file = "humanoid3d_backflip.txt", terrain_file = "plane_implicit.urdf"):

        """
            Load simulation parameters including the:

                - reference motion file on which the simulation model is trained 
                  (and visualized by the kinematic model)
                - terrain file (URDF)

        """

        self.timestep = timestep
        self.motion_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "motions", motion_file)
        self.terrain_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "terrains", terrain_file)
        self.simulation_time = 0

    def enable_draw(self, render):

        """
            Initialize the PyBullet client with:

                - physics configurations
                - simulation and kinematic character models

        """

        # initialize physics client GUI if required & configure physics simulation

        if (render):
            self.pybullet_client = bullet_client.BulletClient(connection_mode = pb.GUI)
        else:
            self.pybullet_client = bullet_client.BulletClient()
        
        self.configure_simulation(self.timestep, self.terrain_file)
            
        # initialize simulation & kinematic models
        self.initialize_models() 
        
        # load reference motion data
        self.load_reference_data(self.motion_file)
        
        # create PD controller for simulation model
        self.stable_PD = pd_controller_stable.PDControllerStableMultiDof(self.pybullet_client)
        
        # reset all components
        self.reset()


    def step(self, action):
        
        """
            Steps the simulation and kinematic characters based on action vector computed by the
            DRL policy. This involves:
            
                - converting the action vector to a desired pose
                - computing the PD forces needed to achieve the desired posed
                - applying the PD forces to the simulation model
                
            The subsequent simulation model state, reward, and termination boolean
            are returned.
        
        """
        
        # step kinematic model first
        self.simulation_time += self.timestep
        self.set_simulation_time(self.simulation_time)
        
        pose = self.compute_pose(self.frame_fraction)
        self.initialize_pose(self.pose_interpolator, self.kinematic_model, initialize_base = True)
        #self.initialize_pose(self.pose_interpolator, self.simulation_model, initialize_base = True)

        # convert action to desired pose
        self.desired_pose = self.convert_action_to_pose(action)

        # compute and apply PD forces 
        taus = self.compute_PD_forces(self.desired_pose, desired_velocities = None, max_forces = self.max_forces)
        self.apply_PD_forces(taus)

        # step simulation
        self.render()

        # compute new observation
        self.observation = self.get_current_state()

        # compute reward
        reward = self.compute_reward()

        # compute whether episode has terminated
        done = self.is_done()

        return np.array(self.observation), reward, done, {}

    def reset(self):
        
        """
            Resets the environment by:
            
                - reinitializing the simulation client
                - reloading the reference data
                - resetting the pose of simulation and kinematic models (to a random starting point)
            
            and returns the state vector to which the environment has been resetted.
        
        """

        # reset simulation
        self.configure_simulation(self.timestep, self.terrain_file)
        
        # reload motion capture data
        self.load_reference_data(self.motion_file)
        
        # reset simulation to random starting time

        #self.set_simulation_time(0)

        random_range = 1000
        random_value = random.randint(0, random_range)
        start_time = float(random_value) / random_range * self.get_cycle_time()

        self.set_simulation_time(start_time)
        
        # reset character
        pose = self.compute_pose(self.frame_fraction)
        self.initialize_pose(self.pose_interpolator, self.simulation_model, initialize_base = True)
        self.initialize_pose(self.pose_interpolator, self.kinematic_model, initialize_base = False)
        
        # save initial state
        self.observation = self.get_current_state()

        return np.array(self.observation)

    def render(self, mode = 'human', close = False):
        
        """
            Defines rendering properties (required by Gym interface).
        
        """
        
        self.pybullet_client.stepSimulation()
        
        return
        

    ################################################################################
    # ============================ Helper Functions ============================== #
    ################################################################################

    def load_model_joint_metadata(self, model_joint_file = "model_joint_metadata.json"):

        model_joint_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), model_joint_file)

        datastore = None

        # load JSON file containing bound values

        with open(model_joint_file, "r") as f:
            datastore = json.load(f)

        self.action_space_size = datastore['space_size']
        self.joint_friction_force = datastore['joint_friction_force']
        self.allowed_body_parts = datastore['allowed_body_parts']
        self.end_effectors = datastore['end_effectors']

        self.joint_indices = {}
        self.joint_Dof_counts = []
        self.max_forces = []
        self.kp_Org, self.kd_Org = [], []
        self.lower_bound, self.upper_bound = [], []

        # concatenate joint values into single lists

        for joint in datastore['joints']:
            self.joint_indices[joint['name']] = joint['index']
            self.joint_Dof_counts.append(joint['DoF'])
            self.max_forces += joint['max_force']
            self.kp_Org += joint['kp_Org']
            self.kd_Org += joint['kd_Org']
            self.lower_bound += joint['rotation_lower']
            self.upper_bound += joint['rotation_upper']

        self.max_forces = [0, 0, 0, 0, 0, 0, 0] + self.max_forces
        self.kp_Org = [0, 0, 0, 0, 0, 0, 0] + self.kp_Org
        self.kd_Org = [0, 0, 0, 0, 0, 0, 0] + self.kd_Org
        self.total_Dofs = 7 + np.sum(np.array(self.joint_Dof_counts))

        return

    ################################################################################
    # =========================== Model Initializers ============================= #
    ################################################################################
    
    def configure_simulation(self, timestep, terrain_file):
        
        self.pybullet_client.configureDebugVisualizer(self.pybullet_client.COV_ENABLE_Y_AXIS_UP, 1)
        self.pybullet_client.configureDebugVisualizer(self.pybullet_client.COV_ENABLE_RGB_BUFFER_PREVIEW, 1)
        self.pybullet_client.setGravity(0,-9.8,0)
        self.pybullet_client.setPhysicsEngineParameter(numSolverIterations = 10)
        self.pybullet_client.setTimeStep(timestep)
        self.pybullet_client.setPhysicsEngineParameter(numSubSteps = 1)
        #self.time_ID = self.pybullet_client.addUserDebugParameter("Time", 0, 10, 0)
        
        pb.setAdditionalSearchPath(pybullet_data.getDataPath())
        z2y = self.pybullet_client.getQuaternionFromEuler([-math.pi * 0.5, 0, 0])
        plane_ID = self.pybullet_client.loadURDF(terrain_file, [0,0,0], z2y, useMaximalCoordinates = True)
        self.pybullet_client.changeDynamics(plane_ID, linkIndex = -1, lateralFriction = 0.9)  
        
        return 


    def initialize_models(self):
        
        ########################## LOAD SIMULATION MODEL ###########################
        
        # define simulation model: character that operates on learned policy

        self.simulation_model = self.pybullet_client.loadURDF(
            "humanoid/humanoid.urdf", 
            [0, 0.85, 0],
            globalScaling = 0.25, 
            useFixedBase = False, 
            flags = self.pybullet_client.URDF_MAINTAIN_LINK_ORDER)

        # set dynamics of simulation model's base (index = -1) and joints

        self.pybullet_client.changeDynamics(self.simulation_model, -1, lateralFriction = 0.9)
        self.pybullet_client.changeDynamics(self.simulation_model, -1, linearDamping = 0, angularDamping = 0)

        for joint in range(self.pybullet_client.getNumJoints(self.simulation_model)):
            self.pybullet_client.changeDynamics(self.simulation_model, joint, lateralFriction = 0.9)
            
        # define motor controls for joints used in this simulation (wrist joints ignored)
        
        for joint in self.joint_indices.keys():
            
            # set both types for each joint, appropriates types are masked later
            
            self.pybullet_client.setJointMotorControl2(self.simulation_model, 
                                                       self.joint_indices[joint], 
                                                       self.pybullet_client.POSITION_CONTROL, 
                                                       targetPosition = 0, 
                                                       positionGain = 0, 
                                                       targetVelocity = 0,
                                                       force = self.joint_friction_force)
            self.pybullet_client.setJointMotorControlMultiDof(self.simulation_model, 
                                                              self.joint_indices[joint], 
                                                              self.pybullet_client.POSITION_CONTROL,
                                                              targetPosition = [0, 0, 0, 1], 
                                                              targetVelocity = [0, 0, 0], 
                                                              positionGain = 0,
                                                              velocityGain = 1,
                                                              force = [self.joint_friction_force,
                                                                       self.joint_friction_force,
                                                                       self.joint_friction_force])
        
        ########################## LOAD KINEMATIC MODEL ############################
        
        self.kinematic_model = self.pybullet_client.loadURDF(
            "humanoid/humanoid.urdf", 
            [0, 0.85, 0],
            globalScaling = 0.25, 
            useFixedBase = True, 
            flags = self.pybullet_client.URDF_MAINTAIN_LINK_ORDER)

        # define dynamics of kinematic model's base (index = -1) and joints

        self.pybullet_client.changeDynamics(self.kinematic_model, -1, linearDamping = 0, angularDamping = 0)
        
        # disable collisions & change link visuals to differentiate between the two characters
        
        self.pybullet_client.setCollisionFilterGroupMask(self.kinematic_model, -1, collisionFilterGroup = 0, collisionFilterMask = 0)
        self.pybullet_client.changeDynamics(self.kinematic_model, -1, 
                                            activationState = self.pybullet_client.ACTIVATION_STATE_SLEEP + 
                                                              self.pybullet_client.ACTIVATION_STATE_ENABLE_SLEEPING + 
                                                              self.pybullet_client.ACTIVATION_STATE_DISABLE_WAKEUP)
        alpha = 0.4
        self.pybullet_client.changeVisualShape(self.kinematic_model, -1, rgbaColor = [1, 1, 1, alpha])

        # define motor controls for joints used in this simulation (wrist joints ignored)
        
        for j in range (self.pybullet_client.getNumJoints(self.kinematic_model)):
            self.pybullet_client.setCollisionFilterGroupMask(self.kinematic_model, j, collisionFilterGroup = 0, collisionFilterMask = 0)
            self.pybullet_client.changeDynamics(self.kinematic_model, j, 
                                                activationState = self.pybullet_client.ACTIVATION_STATE_SLEEP + 
                                                                  self.pybullet_client.ACTIVATION_STATE_ENABLE_SLEEPING + 
                                                                  self.pybullet_client.ACTIVATION_STATE_DISABLE_WAKEUP)
            self.pybullet_client.changeVisualShape(self.kinematic_model, j, rgbaColor = [1, 1, 1, alpha])

        # define motor controls for joints used in this simulation (wrist joints ignored)
            
        for joint in self.joint_indices.keys():
            
            # set both types for each joint, appropriates types are masked later
            
            self.pybullet_client.setJointMotorControl2(self.simulation_model, 
                                                       self.joint_indices[joint], 
                                                       self.pybullet_client.POSITION_CONTROL, 
                                                       targetPosition = 0, 
                                                       positionGain = 0, 
                                                       targetVelocity = 0,
                                                       force = self.joint_friction_force)
            self.pybullet_client.setJointMotorControlMultiDof(self.simulation_model, 
                                                              self.joint_indices[joint], 
                                                              self.pybullet_client.POSITION_CONTROL,
                                                              targetPosition = [0, 0, 0, 1], 
                                                              targetVelocity = [0, 0, 0], 
                                                              positionGain = 0,
                                                              velocityGain = 1,
                                                              force = [self.joint_friction_force,
                                                                       self.joint_friction_force,
                                                                       self.joint_friction_force])
        
        ############################################################################
        
        # define other useful information about characters
        
        #self.end_effectors = [5, 8, 11, 14]  # left and right ankles and wrists
        #self.joint_Dof_counts = [4, 4, 4, 1, 4, 4, 1, 4, 1, 4, 4, 1]  
        #self.allowed_body_parts = [5, 11] # left & right ankles
        #self.total_Dofs = 7 + np.sum(np.array(self.joint_Dof_counts))  
        
        return
    
    def load_reference_data(self, motion_file):
        
        # load motion capture data from motion file
        
        self.motion_data = motion_capture_data.MotionCaptureData()
        self.motion_data.Load(motion_file)
        
        # process motion capture data using PoseInterpolator
    
        self.pose_interpolator = humanoid_pose_interpolator.HumanoidPoseInterpolator()

        for i in range (self.motion_data.NumFrames() - 1):
            frame_data = self.motion_data._motion_data['Frames'][i]
            self.pose_interpolator.PostProcessMotionData(frame_data)
            
        return
    
    def initialize_pose(self, pose, phys_model, initialize_base, initialize_velocity = True):
    
        if initialize_velocity:
            
            if initialize_base:
                self.pybullet_client.resetBasePositionAndOrientation(phys_model, pose._basePos, pose._baseOrn)
                self.pybullet_client.resetBaseVelocity(phys_model, pose._baseLinVel, pose._baseAngVel)
                
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['chest'], pose._chestRot, pose._chestVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['neck'], pose._neckRot, pose._neckVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_hip'], pose._rightHipRot, pose._rightHipVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_knee'], pose._rightKneeRot, pose._rightKneeVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_ankle'], pose._rightAnkleRot, pose._rightAnkleVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_shoulder'], pose._rightShoulderRot, pose._rightShoulderVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_elbow'], pose._rightElbowRot, pose._rightElbowVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_hip'], pose._leftHipRot, pose._leftHipVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_knee'], pose._leftKneeRot, pose._leftKneeVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_ankle'], pose._leftAnkleRot, pose._leftAnkleVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_shoulder'], pose._leftShoulderRot, pose._leftShoulderVel)
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_elbow'], pose._leftElbowRot, pose._leftElbowVel)
        
        else:
            
            if initialize_base:
                self.pybullet_client.resetBasePositionAndOrientation(phys_model, pose._basePos, pose._baseOrn)
                
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['chest'], pose._chestRot, [0,0,0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['neck'], pose._neckRot, [0,0,0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_hip'], pose._rightHipRot, [0,0,0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_knee'], pose._rightKneeRot, [0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_ankle'], pose._rightAnkleRot, [0,0,0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_shoulder'], pose._rightShoulderRot, [0,0,0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['right_elbow'], pose._rightElbowRot, [0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_hip'], pose._leftHipRot, [0,0,0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_knee'], pose._leftKneeRot, [0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_ankle'], pose._leftAnkleRot, [0,0,0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_shoulder'], pose._leftShoulderRot, [0,0,0])
            self.pybullet_client.resetJointStateMultiDof(phys_model, self.joint_indices['left_elbow'], pose._leftElbowRot, [0])
        
        return
        
    ################################################################################
    # ========================= Kinetic Model Methods ============================ #
    ################################################################################
    
    def compute_pose(self, frame_fraction):
        
        # compute current and next frame data
        
        frame_data = self.motion_data._motion_data['Frames'][self.frame]
        frame_data_next = self.motion_data._motion_data['Frames'][self.frame_next]
        
        # interpolate between frames

        self.pose_interpolator.Slerp(frame_fraction, frame_data, frame_data_next, self.pybullet_client)
        self.compute_cycle_offset()
        old_pos = self.pose_interpolator._basePos
        
        self.pose_interpolator._basePos = [old_pos[0] + self.cycle_count * self.cycle_offset[0],
                                           old_pos[1] + self.cycle_count * self.cycle_offset[1],
                                           old_pos[2] + self.cycle_count * self.cycle_offset[2]]
                
        return self.pose_interpolator.GetPose()
    
    def compute_cycle_offset(self):
        
        first_frame = 0
        last_frame = self.motion_data.NumFrames() - 1
        
        frame_data = self.motion_data._motion_data['Frames'][0]
        frame_data_next = self.motion_data._motion_data['Frames'][last_frame]

        base_pos_start = [frame_data[1], frame_data[2], frame_data[3]]
        base_pos_end = [frame_data_next[1], frame_data_next[2], frame_data_next[3]]
        
        self.cycle_offset = [base_pos_end[0] - base_pos_start[0],
                             base_pos_end[1] - base_pos_start[1],
                             base_pos_end[2] - base_pos_start[2]]
        
        return self.cycle_offset
    
    def get_cycle_time(self):
        
        key_frame_duration = self.motion_data.KeyFrameDuraction()
        cycle_time = key_frame_duration * (self.motion_data.NumFrames() - 1)
        
        return cycle_time
    
    def calculate_cycle_count(self, simulation_time, cycle_time):
        
        phases = simulation_time / cycle_time;
        count = math.floor(phases)

        return count
    
    def set_simulation_time(self, t):
        
        self.simulation_time = t
        key_frame_duration = self.motion_data.KeyFrameDuraction()
        cycle_time = self.get_cycle_time()
        self.cycle_count = self.calculate_cycle_count(t, cycle_time)
        frame_time = t - self.cycle_count * cycle_time
        
        if (frame_time < 0):
            frame_time += cycle_time

        self.frame = int(frame_time / key_frame_duration)
        self.frame_next = self.frame + 1
        
        if (self.frame_next >=  self.motion_data.NumFrames()):
          self.frame_next = self.frame

        self.frame_fraction = (frame_time - self.frame * key_frame_duration) / (key_frame_duration)
        
        return
        
    ################################################################################
    # ==================== Simulation Model Action Methods ======================= #
    ################################################################################

    def convert_action_to_pose(self, action):
        
        """
            Converts action vector to a desired position vector.
            
        """
        
        # use pose interpolator to compute pose from action vector
        pose = self.pose_interpolator.ConvertFromAction(self.pybullet_client, action)
        
        # zero the target root position (x, y, z) and orientation (w, x, y, z)
        for i in range(7):
            pose[i] = 0
        
        return pose  

    def compute_PD_forces(self, desired_positions, desired_velocities, max_forces):
        
        """
            Computes PD controller forces given desired pose (as determined by the 
            action taken). This method accepts additional parameters including:
            
                - max forces to which the joint forces are limited
                - desired velocities is available
                
        """
        
        if desired_velocities == None:
            desired_velocities = [0] * self.total_Dofs
            
        # use PD controller to determine forces
        
        taus = self.stable_PD.computePD(bodyUniqueId = self.simulation_model, 
                                        jointIndices = list(self.joint_indices.values()),
                                        desiredPositions = desired_positions,
                                        desiredVelocities = desired_velocities,
                                        kps = self.kp_Org,
                                        kds = self.kd_Org,
                                        maxForces = max_forces, 
                                        timeStep = self.timestep)
        
        return taus

    def apply_PD_forces(self, taus):
        
        """
            Loops through the computed PD forces and applies them appropriately
            to the corresponding simulation model joints.
        
        """

        dofIndex, scaling, index = 7, 1, 0
        
        for joint_index in list(self.joint_indices.values()):
                    
            if self.joint_Dof_counts[index] == 4:
                
                force = [scaling * taus[dofIndex + 0],
                         scaling * taus[dofIndex + 1],
                         scaling * taus[dofIndex + 2]]
                
                self.pybullet_client.setJointMotorControlMultiDof(self.simulation_model,
                                                                  joint_index,
                                                                  self.pybullet_client.TORQUE_CONTROL,
                                                                  force = force)
          
            if self.joint_Dof_counts[index] == 1:
                
                force = [scaling * taus[dofIndex]]
                
                self.pybullet_client.setJointMotorControlMultiDof(self.simulation_model, 
                                                                  joint_index, 
                                                                  controlMode = self.pybullet_client.TORQUE_CONTROL, 
                                                                  force = force)
            
            dofIndex += self.joint_Dof_counts[index]
            index += 1
            
        return
    
    ################################################################################
    # ===================== Simulation Model State Methods ======================= #
    ################################################################################

    def get_current_state(self):

        """
            Return the simulation model's current state vector (197 dimensional). 
            This includes:
            
            ==================================================================
                       INDEX | OBSERVATION ATTRIBUTE
            ==================================================================
                           0 | phase
                           1 | rootPosRel[1]
                     2 - 106 | FOR all joints (15):
                             |      local_link_position (x, y, z)
                             |      local_link_orientation (w, x, y, z)
                   107 - 196 | FOR all joints (15):
                             |      local_link_linear_velocity (x, y, z)
                             |      local_link_angular_velocity (x, y, z)
            ==================================================================

        """
        
        state_vector = []
        
        # INDEX 0 ==> phase
        
        phase = self.get_phase()
        state_vector.append(phase)
        
        # INDEX 1 ==> relative root height (y)

        root_trans_pos, root_trans_orn = self.build_origin_trans()
        base_pos, base_orn = self.pybullet_client.getBasePositionAndOrientation(self.simulation_model)
        root_pos_rel, _ = self.pybullet_client.multiplyTransforms(root_trans_pos, root_trans_orn, base_pos, [0,0,0,1])
        local_pos, local_orn = self.pybullet_client.multiplyTransforms(root_trans_pos, root_trans_orn, base_pos, base_orn)
        
        local_pos = [local_pos[0] - root_pos_rel[0],
                     local_pos[1] - root_pos_rel[1],
                     local_pos[2] - root_pos_rel[2]]

        state_vector.append(root_pos_rel[1])
        
        # INDICES 2:106 ==> joint local position (x, y, z) and orientations (w, x, y, z)

        self.pb2dmJoints = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]

        for pb_joint in range(self.pybullet_client.getNumJoints(self.simulation_model)):
            
            # get global joint state values
            
            j = self.pb2dmJoints[pb_joint]
            ls = self.pybullet_client.getLinkState(self.simulation_model, j, computeForwardKinematics = True)
            link_pos, link_orn = ls[0], ls[1]
            
            # compute local joint state values
            
            link_pos_local, link_orn_local = self.pybullet_client.multiplyTransforms(root_trans_pos, root_trans_orn, link_pos, link_orn)
            
            if (link_orn_local[3] < 0):
                link_orn_local = [-link_orn_local[0], -link_orn_local[1], -link_orn_local[2], -link_orn_local[3]]
            
            link_pos_local = [link_pos_local[0] - root_pos_rel[0],
                              link_pos_local[1] - root_pos_rel[1],
                              link_pos_local[2] - root_pos_rel[2]]
            
            for l in link_pos_local:
                state_vector.append(l)
                
            # re-order the quaternion since DeepMimic uses (w, x, y, z)
            
            if (link_orn_local[3] < 0):
                link_orn_local[0] *= -1
                link_orn_local[1] *= -1
                link_orn_local[2] *= -1
                link_orn_local[3] *= -1

            state_vector.append(link_orn_local[3])
            state_vector.append(link_orn_local[0])
            state_vector.append(link_orn_local[1])
            state_vector.append(link_orn_local[2])
    
        # INDICES 107:196 ==> joint local linear and angular velocities (x, y, z)

        for pb_joint in range(self.pybullet_client.getNumJoints(self.simulation_model)):
            
            j = self.pb2dmJoints[pb_joint]
            ls = self.pybullet_client.getLinkState(self.simulation_model, j, computeLinkVelocity = True)
            
            # get global joint velocity values
            
            link_lin_vel = ls[6]
            link_ang_vel = ls[7]
            
            # get local joint velocity values
            
            link_lin_vel_local, _ = self.pybullet_client.multiplyTransforms([0,0,0], root_trans_orn, link_lin_vel, [0,0,0,1])
            link_ang_vel_local, _ = self.pybullet_client.multiplyTransforms([0,0,0], root_trans_orn, link_ang_vel, [0,0,0,1])

            for l in link_lin_vel_local:
                state_vector.append(l)
            for l in link_ang_vel_local:
                state_vector.append(l)
                
        return state_vector
    
    def get_phase(self):
    
        key_frame_duration = self.motion_data.KeyFrameDuraction()
        cycle_time = key_frame_duration * (self.motion_data.NumFrames() - 1)
        phase = self.simulation_time / cycle_time
        phase = math.fmod(phase, 1.0)
        
        if (phase < 0):
          phase += 1
        
        return phase
    
    def build_origin_trans(self):
        
        root_pos, root_orn = self.pybullet_client.getBasePositionAndOrientation(self.simulation_model)
        inv_root_pos = [-root_pos[0], 0, -root_pos[2]]
        heading_orn = self.build_heading_trans(root_orn)
        heading_mat = self.pybullet_client.getMatrixFromQuaternion(heading_orn)

        inv_orig_trans_pos, inv_orig_trans_orn = self.pybullet_client.multiplyTransforms([0,0,0], heading_orn, inv_root_pos, [0,0,0,1])
        inv_orig_trans_mat = self.pybullet_client.getMatrixFromQuaternion(inv_orig_trans_orn)

        return inv_orig_trans_pos, inv_orig_trans_orn
    
    def build_heading_trans(self, root_orn):

        eul = self.pybullet_client.getEulerFromQuaternion(root_orn)
        ref_dir = [1,0,0]
        rot_vec = self.pybullet_client.rotateVector(root_orn, ref_dir)
        heading = math.atan2(-rot_vec[2], rot_vec[0])
        heading2 = eul[1]
        heading_orn = self.pybullet_client.getQuaternionFromAxisAngle([0,1,0],-heading)
        
        return heading_orn
    
    def is_done(self):

        # check if any non-allowed body part hits the ground
        
        terminates = False
        parts = self.pybullet_client.getContactPoints()
        
        for p in parts:
            
            part = -1
            
            # ignore self-collision
            
            if (p[1] == p[2]):
                continue
            if (p[1] == self.simulation_model):
                part = p[3]
            if (p[2] == self.simulation_model):
                part = p[4]
            if (part >=0 and part not in self.allowed_body_parts):
                terminates = True

        return terminates
    
    ################################################################################
    # ===================== Simulation Model Reward Methods ====================== #
    ################################################################################
    
    def compute_reward(self):
        
        pose_w = 0.5
        vel_w = 0.05
        end_eff_w = 0.15
        root_w = 0.2
        com_w = 0 #0.1

        total_w = pose_w + vel_w + end_eff_w + root_w + com_w
        pose_w /= total_w
        vel_w /= total_w
        end_eff_w /= total_w
        root_w /= total_w
        com_w /= total_w

        pose_scale = 2
        vel_scale = 0.1
        end_eff_scale = 40
        root_scale = 5
        com_scale = 10
        err_scale = 1

        reward = 0

        pose_err = 0
        vel_err = 0
        end_eff_err = 0
        root_err = 0
        com_err = 0
        heading_err = 0

        root_id = 0

        mJointWeights = [0.20833,0.10416, 0.0625, 0.10416,
          0.0625, 0.041666666666666671, 0.0625, 0.0416,
          0.00, 0.10416,  0.0625, 0.0416, 0.0625, 0.0416, 0.0000]

        num_end_effs = 0
        num_joints = 15

        root_rot_w = mJointWeights[root_id]
        rootPosSim,rootOrnSim = self.pybullet_client.getBasePositionAndOrientation(self.simulation_model)
        rootPosKin ,rootOrnKin = self.pybullet_client.getBasePositionAndOrientation(self.kinematic_model)
        linVelSim, angVelSim = self.pybullet_client.getBaseVelocity(self.simulation_model)
        linVelKin, angVelKin = self.pybullet_client.getBaseVelocity(self.kinematic_model)

        root_rot_err = self.calcRootRotDiff(rootOrnSim,rootOrnKin)
        pose_err += root_rot_w * root_rot_err

        root_vel_diff  = [linVelSim[0]-linVelKin[0],linVelSim[1]-linVelKin[1],linVelSim[2]-linVelKin[2]]
        root_vel_err = root_vel_diff[0]*root_vel_diff[0]+root_vel_diff[1]*root_vel_diff[1]+root_vel_diff[2]*root_vel_diff[2]

        root_ang_vel_err = self.calcRootAngVelErr( angVelSim, angVelKin)
        vel_err += root_rot_w * root_ang_vel_err

        for j in range (num_joints):
            
            curr_pose_err = 0
            curr_vel_err = 0
            w = mJointWeights[j];

            simJointInfo = self.pybullet_client.getJointStateMultiDof(self.simulation_model, j)
            kinJointInfo = self.pybullet_client.getJointStateMultiDof(self.kinematic_model,j)

            if (len(simJointInfo[0])==1):
                angle = simJointInfo[0][0]-kinJointInfo[0][0]
                curr_pose_err = angle*angle
                velDiff = simJointInfo[1][0]-kinJointInfo[1][0]
                curr_vel_err = velDiff*velDiff
                
            if (len(simJointInfo[0])==4):
                diffQuat = self.pybullet_client.getDifferenceQuaternion(simJointInfo[0],kinJointInfo[0])
                axis,angle = self.pybullet_client.getAxisAngleFromQuaternion(diffQuat)
                curr_pose_err = angle*angle
                diffVel = [simJointInfo[1][0]-kinJointInfo[1][0],simJointInfo[1][1]-kinJointInfo[1][1],simJointInfo[1][2]-kinJointInfo[1][2]]
                curr_vel_err = diffVel[0]*diffVel[0]+diffVel[1]*diffVel[1]+diffVel[2]*diffVel[2]

            pose_err += w * curr_pose_err
            vel_err += w * curr_vel_err

            is_end_eff = j in self.end_effectors
            
            if is_end_eff:

                linkStateSim = self.pybullet_client.getLinkState(self.simulation_model, j)
                linkStateKin = self.pybullet_client.getLinkState(self.kinematic_model, j)
                linkPosSim = linkStateSim[0]
                linkPosKin = linkStateKin[0]
                linkPosDiff = [linkPosSim[0]-linkPosKin[0],linkPosSim[1]-linkPosKin[1],linkPosSim[2]-linkPosKin[2]]
                curr_end_err = linkPosDiff[0]*linkPosDiff[0]+linkPosDiff[1]*linkPosDiff[1]+linkPosDiff[2]*linkPosDiff[2]
                end_eff_err += curr_end_err
                num_end_effs+=1

        if (num_end_effs > 0):
          end_eff_err /= num_end_effs

        root_pos_diff = [rootPosSim[0]-rootPosKin[0],rootPosSim[1]-rootPosKin[1],rootPosSim[2]-rootPosKin[2]]
        root_pos_err = root_pos_diff[0]*root_pos_diff[0]+root_pos_diff[1]*root_pos_diff[1]+root_pos_diff[2]*root_pos_diff[2]

        root_err = root_pos_err + 0.1 * root_rot_err+ 0.01 * root_vel_err+ 0.001 * root_ang_vel_err

        pose_reward = math.exp(-err_scale * pose_scale * pose_err)
        vel_reward = math.exp(-err_scale * vel_scale * vel_err)
        end_eff_reward = math.exp(-err_scale * end_eff_scale * end_eff_err)
        root_reward = math.exp(-err_scale * root_scale * root_err)
        com_reward = math.exp(-err_scale * com_scale * com_err)

        reward = pose_w * pose_reward + vel_w * vel_reward + end_eff_w * end_eff_reward + root_w * root_reward + com_w * com_reward

        return reward
    
    def quatMul(self, q1, q2):
        return [ q1[3] * q2[0] + q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1],
               q1[3] * q2[1] + q1[1] * q2[3] + q1[2] * q2[0] - q1[0] * q2[2],
              q1[3] * q2[2] + q1[2] * q2[3] + q1[0] * q2[1] - q1[1] * q2[0],
              q1[3] * q2[3] - q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2]]
              
    def calcRootAngVelErr(self, vel0, vel1):
        diff = [vel0[0]-vel1[0],vel0[1]-vel1[1], vel0[2]-vel1[2]]
        return diff[0]*diff[0]+diff[1]*diff[1]+diff[2]*diff[2]
    
    def calcRootRotDiff(self,orn0, orn1):
        orn0Conj = [-orn0[0],-orn0[1],-orn0[2],orn0[3]]
        q_diff = self.quatMul(orn1, orn0Conj)
        axis,angle = self.pybullet_client.getAxisAngleFromQuaternion(q_diff)
        return angle*angle