import logging
from gym.envs.registration import register

logger = logging.getLogger(__name__)

register(
    id='DeepMimicHumanoid-v0',
    entry_point='deepmimic_humanoid.envs:DeepMimicHumanoid',
)
