**PRE-REQUISITES**

	Python >= 3.6 (recommended install through Anaconda)
	Gcc/G++

**1. SETUP**

    1.1. Clone repository to local file system:
    
        $ git clone https://gitlab.com/abhijeet.jagdev/pybullet_humanoid_simulator.git
        $ cd /path/to/.../pybullet_humanoid_simulator/
        
    1.2. Create and activate python virtual environment to manage packages:
    
	$ pip install virtualenv
        $ python -m virtualenv env
        $ source env/bin/activate
            
            or on Windows
            
        $ env/Scripts/activate
    
    1.3. Install dependencies:
    
        $ pip install -e .

    1.4. Install stable-baselines:

	Follow the installation guide here https://stable-baselines.readthedocs.io/en/master/guide/quickstart.html for the appropriate platform.

	$ pip install stable-baselines

**2. MOTION PLAYER**

    2.1. Change into motion player directory (assuming current directory is ".../pybullet_humanoid_simulator/"):
    
        $ cd motion_player
    
    2.2. Display options:
    
        $ python motion_player.py -h
    
    2.3. Configure options and run:
    
        e.g. $ python motion_player.py -M ./motions/humanoid3d_cartwheel.txt -t 200
        
    2.4. Simulation controls:
    
         SPACE: pause/play
             X: continuously step forwards
             Z: continuously step backwards
             S: step forwards once
             A: step backwards once
           ESC: exit simulation

**3. HUMANOID TRAINER**

    3.1 Display options (assuming current directory is ".../pybullet_humanoid_simulator/"):
    
        $ python main.py -h
        
    3.2 To train policy, run in mode 'train' (default mode):
    
        e.g. $ python main.py --env balancebot-v0 --timesteps 1000000 --algorithm TRPO --max_steps 500
        
    3.3 Monitor training:

	Launch a new terminal window.
    
        e.g. $ tensorboard --logdir=./logs/balancebot-v0/PPO/2019_05_03_05_06_21/tensorboard/

	Navigate to "localhost:6006" from browser (port may be different, check terminal logs).
        
    3.4 Play trained policy:
    
        e.g. $ python main.py --env balancebot-v0 --algorithm TRPO --mode play --model_path ./logs/balancebot-v0/PPO/2019_05_03_05_06_21/final_model.pkl
        
    ***Note:** running "main.py" with "python ..." may not work with an Anaconda installation of Python3.6 on MacOS. Instead use "pythonw ..."

